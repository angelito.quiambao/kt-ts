type employeeIds = Array<Number>;
let staffIds: employeeIds = [202201, 202202, 202203];

interface Employee {
    id: number,
    name: string,
    position: string,
    address?: string
}

function checkEmployment(employee: Employee): string{
    if (typeof employee == undefined){
        return "You're not an employee!";
    }
    else{
        if(staffIds.includes(employee.id)){
            return "Welcome back " + employee.name + "!";
        }
        return "Employee ID not found!";
    }
}

let manager: Employee = {
    id: 202201,
    name: "John Smith",
    position: "Manager",
    address: "Quezon City, Metro Manila"
}

console.log(checkEmployment(manager));