var staffIds = [202201, 202202, 202203];
function checkEmployment(employee) {
    if (typeof employee == undefined) {
        return "You're not an employee!";
    }
    else {
        if (staffIds.includes(employee.id)) {
            return "Welcome back " + employee.name + "!";
        }
        return "Employee ID not found!";
    }
}
var manager = {
    id: 202201,
    name: "John Smith",
    position: "Manager",
    address: "Quezon City, Metro Manila"
};
console.log(checkEmployment(manager));
