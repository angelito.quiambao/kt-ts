function hello(){
    console.log("Hello, TineScript");
}

hello();

// To create variable in TS, we also use the let keyword.
// However, unlike in Java, we don't have to indicate the type of data.
let myNumber = 1;

// In typescript, We cannot update a variable to a different type:
// myNumber = "Hello";
// console.log(myNumber);

//Annotation in TS
//Annotation in TS, allows us ti add and enforce type checking for our variables:
//let varName: annotation;
let myNum: number;
myNum = 25;
//myNum = true
let myString: string;
//myString = {}
let myBool: boolean;
//myBool = "true"

//What if we want to create a variable that takes any type?
//let sample = 1;
//sample = "";
let unknownVar: any; // this variable is now able to take any type:
unknownVar = "unknown is my favorite pokemon";
console.log(unknownVar);
unknownVar = 12;

// we can slo add annotations to our arrays to limit them to a certain data type:
let myNumArr: number[];
myNumArr = [1, 2, 3];
// myNumArr = [1, "2", "3"]; // error because of the set annotation
// myNumArr.push("Hello"); // array methods will not also work when different data type

myNumArr.push(6);
console.log(myNumArr);

//We can still create an array of any type:
let otherArr: any[] = ["John", 31, true];

// We can also still use typeof from JS to check the data type of a variable's data.
console.log(typeof unknownVar);
console.log(typeof myNumArr);

// Annotations in Functions
// Annotations can also be used to ensure the data type of the parameter or the return value:

function greet(name: string){
    return "Hello, " + name + ", Welcome to the other side.";
}

// greet(25);
console.log(greet("Tolits"));

function add(num1:number, num2:number){
    return num1+num2;
}

//We can aslo add annotaions for the return value of a function
function sayMyName(name:string):string {

    return "You are" + " " + name + ".";

}

//sayMyName(25);
sayMyName("Beyonce");

//Generics
//Allow us to create components/functions that can work over a variaty of types rather than a single one;
function genericFunctions <T>(param: T): T{
    return param;
}

let result1 = genericFunctions("Hello");
let result2 = genericFunctions(25);

console.log(result1);
console.log(result2);

//Use generics when creating an array:
type numArray = Array<number>;
let numberArray: numArray = [1,4,3];
// numberArray.push(true);

//Interface and Type Alias
// Interface represents the shapre of an object-like structure.
// Enforce object creation to conform to a defined structure or the shape of an indicated interface.

interface User{
    username: string,
    password: string,
    age: number,
    guildName?: string // "?" denotes an optional property
}

// let user1: User = {
//     username: "knight99",
//     password: "1234",
//     age: 15
// }

// let user2: User ={
//     username: "full_support19",
//     password: "!1234",
//     age: 20,
//     guildName: "Adobo Boys"
// }

//Type Alias
// allow us to define/create names for different data type:
type Message = string;
let notification: Message;
// notification = true;

//You could also use Type Alias like an interface to enforce conforming an object to a certain shape or structure

type userAlias ={

    name: string;
    age: number;
    address?: string;
    income?: number;
    expenses?: number;

}

let newUser: userAlias = {
    name: "Prince Ali",
    age: 16
}

// You can actually combine interface with the same name:
interface User{
    email: string
}

let user3: User = {
    username: "noobie191",
    password: "!234",
    age: 15,
    email: "noobie1991@mail.com"
}

//type UserAlias = {} // cannot add functionalities

function getUser(): User{
    let user:User ={
        username: "mynameisjeff",
        age: 15,
        password: "jumpst22",
        email: "channing@mail.com"
    }

    return user;
}

console.log(getUser());

// We can also use type Alias to annotate our parameters and enforce the shape/datatype of the parameter:

type Animal ={
    name: string;
    habitat: string;
}

function getAnimalHabitat(animal: Animal): string{
    return animal.name + " lives in " + animal.habitat;
}

console.log(getAnimalHabitat({
    name: "Lion",
    habitat: "Savannah"
}));

//array <T> with Type Alias
let zooAnimals: Array<Animal> =[
    {
        name: "Tiger",
        habitat: "Jungle"
    },
    {
        name: "Orangutan",
        habitat: "Jungle"
    },
    {
        name: "Python",
        habitat: "Jungle"
    }
];